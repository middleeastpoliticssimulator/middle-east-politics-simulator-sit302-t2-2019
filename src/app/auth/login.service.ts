import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { appUser } from '../models/user';
import { switchMap } from 'rxjs/operators';
import { country } from '../models/country';
import { role } from '../models/role';
import { stringify } from 'querystring';
import { tweetClass } from '../models/tweetClass';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private eventAuthError = new BehaviorSubject<string>("");
  eventAuthError$ = this.eventAuthError.asObservable();
  newUser: appUser;

  user$: Observable<appUser>;
  userList$: Observable<appUser[]>;
  countries$: Observable<country[]>;
  roles$: Observable<role[]>;
  tweetSend: tweetClass;
  tweets$: Observable<tweetClass[]>;


  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private router: Router
  ) {
    this.countries$ = this.db.collection('Countries').valueChanges();
    this.tweets$ = this.db.collection('Tweets').valueChanges();
    this.roles$ = this.db.collection('Roles').valueChanges();
    this.userList$ = this.db.collection('Users').valueChanges();
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.db.doc<appUser>(`Users/${user.uid}`).valueChanges()
        } else {
          return of(null)
        }
      })
    )
  }

  getUserCurrent() {
    return this.afAuth.authState;
  }

  login(email: string, password: string) {
    this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .catch(error => {
        this.eventAuthError.next(error);
      })
      .then(userCredential => {
        if (userCredential) {
          this.router.navigate(['/home']);
        }
      })
  }



  createControlUser(user) {
    this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
      .then(userCredential => {
        this.newUser = user

        console.log(userCredential);
        userCredential.user.updateProfile({
          displayName: user.firstName + ' ' + user.lastName
        });

        this.sendControlUserData(userCredential)
          .then(() => {
            this.router.navigate(['/home'])
          });
      })
      .catch(error => {
        this.eventAuthError.next(error);
      })
  }

  createParticipantUser(user) {
    this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
      .then(userCredential => {
        this.newUser = user

        console.log(userCredential);
        userCredential.user.updateProfile({
          displayName: user.firstName + ' ' + user.lastName
        });

        this.sendParticipantUserData(userCredential)
          .then(() => {
            this.router.navigate(['/home'])
          });
      })
      .catch(error => {
        this.eventAuthError.next(error);
      })
  }


  sendControlUserData(userCredential: firebase.auth.UserCredential) {
    return this.db.doc(`Users/${userCredential.user.uid}`).set({
      email: this.newUser.email,
      firstName: this.newUser.firstName,
      lastName: this.newUser.lastName,
      role: 'Control',
      systemRole: 'admin'
    })
  }

  sendParticipantUserData(userCredential: firebase.auth.UserCredential) {
    return this.db.doc(`Users/${userCredential.user.uid}`).set({
      email: this.newUser.email,
      firstName: this.newUser.firstName,
      lastName: this.newUser.lastName,
      role: this.newUser.role,
      systemRole: 'participant'
    })
  }

  sendCountryData(newCountry: country) {
    this.router.navigate(['/home'])
    return this.db.collection(`Countries`).add({
      countryName: newCountry.countryName
    })
  }

  sendRoleData(newRole: role) {
    this.router.navigate(['/home'])
    return this.db.collection(`Roles`).add({
      roleName: newRole.roleName,
      ofCountry: newRole.ofCountry
    })
  }

  sendTweetData(newTweet: tweetClass) {
    console.log('test');
    console.log(newTweet);
    return this.db.collection(`Tweets`).add({
      userName: newTweet.userName,
      userEmail: newTweet.userEmail,
      userRole: newTweet.userRole,
      tweetDescription: newTweet.tweetDescription,
      tweetDate: newTweet.tweetDate
    })
  }

  getRoles() {
    return this.roles$;
  }

  getTweets() {
    return this.tweets$;
  }

  getCountries() {
    return this.countries$;
  }

  getUserList() {
    return this.userList$;
  }

  logout() {
    return this.afAuth.auth.signOut();
  }
}
