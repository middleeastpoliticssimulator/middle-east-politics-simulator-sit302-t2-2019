export interface appUser {
    id?:string;
    firstName?:string;
    lastName?: string;
    email?: string;
    systemRole?: string;
    role?: string;
  }