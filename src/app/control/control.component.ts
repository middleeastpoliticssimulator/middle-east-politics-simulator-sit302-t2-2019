import { Component, OnInit } from '@angular/core';
import { LoginService } from '../auth/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.scss']
})
export class ControlComponent implements OnInit {

  user: firebase.User;

  constructor(private auth: LoginService,
    private router: Router) { }

  ngOnInit() {this.auth.getUserCurrent()
    .subscribe(user => {
    this.user = user;
  })
  }

  goToCountryTool() {
    this.router.navigate(['/countryTool']);
  }

  goToRoleTool() {
    this.router.navigate(['/roleTool']);
  }

  goToParticipantTool() {
    this.router.navigate(['/participantTool']);
  }
}
